// Bogdan Ivan, 2032824

import javafx.application.*;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;


public class RpsApplication extends Application {
    private RpsGame game = new RpsGame();
    	
	public void start(Stage stage) {
		Group root = new Group();
        
        // Buttons field
        HBox buttons = new HBox();
        Button rockButton = new Button("rock");
        Button paperButton = new Button("paper");
        Button scissorsButton = new Button("scissors");

        // Text Field
        HBox textFields = new HBox();
        TextField result = new TextField("Welcome!");
        TextField winsField = new TextField("wins: " + game.getWins());
        TextField tiesField = new TextField("ties: " + game.getTies());
        TextField lossesField = new TextField("losses: " + game.getLosses());

        // Changing width of results field in the textFields HBox
        result.setPrefWidth(200);

        // Adding event listeners to the buttons
        RpsChoice rockChoice = new RpsChoice(result, winsField, tiesField, lossesField, "rock", game);
        RpsChoice paperChoice = new RpsChoice(result, winsField, tiesField, lossesField, "paper", game);
        RpsChoice scissorsChoice = new RpsChoice(result, winsField, tiesField, lossesField, "scissors", game);

        rockButton.setOnAction(rockChoice);
        paperButton.setOnAction(paperChoice);
        scissorsButton.setOnAction(scissorsChoice);

        // Adding buttons and text fields to their respective boxes
        buttons.getChildren().addAll(rockButton, paperButton, scissorsButton);
        textFields.getChildren().addAll(result, winsField, tiesField, lossesField);

        // Putting both HBoxes in a VBox
        VBox overall = new VBox();
        overall.getChildren().addAll(buttons, textFields);

        root.getChildren().add(overall);

		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 650, 300); 
		scene.setFill(Color.BLACK);

		//associate scene to stage and show
		stage.setTitle("Rock Paper Scissors"); 
		stage.setScene(scene); 
		
		stage.show(); 
	}
	
    public static void main(String[] args) {
        Application.launch(args);
    }
}    
