// Bogdan Ivan, 2032824

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;

public class RpsChoice implements EventHandler<ActionEvent> {
    private TextField message;
    private TextField winsField;
    private TextField tiesField;
    private TextField lossesField;
    private String choice;
    private RpsGame game;

    public RpsChoice(TextField message, TextField winsField, TextField tiesField, TextField lossesField, String choice, RpsGame game) {
        this.message = message;
        this.winsField = winsField;
        this.tiesField = tiesField;
        this.lossesField = lossesField;
        this.choice = choice;
        this.game = game;
    }

    @Override
    public void handle(ActionEvent e) {
        // Stores result of the game in a variable
        String result = game.playRound(choice);
        
        // Append game information in respective fields
        message.setText(result);
        winsField.setText("wins: " + String.valueOf(game.getWins()));
        tiesField.setText("ties: " + String.valueOf(game.getTies()));
        lossesField.setText("losses: " + String.valueOf(game.getLosses()));
    }
}
