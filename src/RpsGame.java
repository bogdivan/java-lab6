// Bogdan Ivan, 2032824
import java.util.Random;

public class RpsGame {
    private int wins;
    private int ties;
    private int losses;
    private Random r;

    public RpsGame() {
        wins = 0;
        ties = 0;
        losses = 0;
        r = new Random();
    }

    public int getWins() {
        return this.wins;
    }

    public int getTies() {
        return this.ties;
    }

    public int getLosses() {
        return this.losses;
    }

    public String playRound(String choice) {
        int computerChoice = r.nextInt(3);

        // 0 is rock, 1 is paper, 2 is scissors
        switch (choice) {
            case "rock":
                if (computerChoice == 0) {
                    ties++;
                    return "Computer: rock. Nobody won...";
                }
                else if (computerChoice == 1) {
                    losses++;
                    return "Computer: paper. You lost!";
                }
                else if (computerChoice == 2) {
                    wins++;
                    return "Computer: scissors. You won!";
                }
            case "paper":
                if (computerChoice == 0) {
                    wins++;
                    return "Computer: rock. You won!";
                }
                else if (computerChoice == 1) {
                    ties++;
                    return "Computer: paper. Nobody won...";
                }
                else if (computerChoice == 2) {
                    losses++;
                    return "Computer: scissors. You lost!";
                }
            case "scissors":
                if (computerChoice == 0) {
                    losses++;
                    return "Computer: rock. You lost!";
                }
                else if (computerChoice == 1) {
                    wins++;
                    return "Computer: paper. You won!";
                }
                else if (computerChoice == 2) {
                    ties++;
                    return "Computer: scissors. Nobody won...";
                }
        }
        return null;
    } 
}